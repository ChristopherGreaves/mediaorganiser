﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediaOrganiser.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace MediaOrganiser.Controllers
{
    public class MediaController : Controller
    {
        // GET: Media/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Media/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                List<MediaFile> files = null;
                if (!HttpContext.Session.TryGetValue("_files", out var filesBytes))
                {
                    files = new List<MediaFile>();
                }
                else
                {
                    files = JsonConvert.DeserializeObject<List<MediaFile>>(Encoding.ASCII.GetString(filesBytes));
                }

                files.Add(new MediaFile
                {
                    Id = files.Count + 1,
                    FileName = collection["FileName"],
                    Path = collection["Path"],
                    Type = collection["Type"],
                    Comments = collection["Comments"]
                });

                HttpContext.Session.Set("_files", Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(files)));

                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return View();
            }
        }

        // GET: Media/Edit/filename
        public ActionResult Edit(int id)
        {
            MediaFile file = null;
            if (HttpContext.Session.TryGetValue("_files", out var filesBytes))
            {
                var files = JsonConvert.DeserializeObject<List<MediaFile>>(Encoding.ASCII.GetString(filesBytes));
                file = files.SingleOrDefault(x => x.Id == id);
            }

            return View("Edit", file);
        }

        // POST: Media/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                if (!HttpContext.Session.TryGetValue("_files", out var filesBytes))
                {
                    return Edit(id);
                }
                var files = JsonConvert.DeserializeObject<List<MediaFile>>(Encoding.ASCII.GetString(filesBytes));
                var file = files.SingleOrDefault(x => x.Id == id);

                file.FileName = collection["FileName"];
                file.Path = collection["Path"];
                file.Type = collection["Type"];
                file.Comments = collection["Comments"];

                HttpContext.Session.Set("_files", Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(files)));

                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return Edit(id);
            }
        }

        // GET: Media/Delete/5
        public ActionResult Delete(int id)
        {
            if (!HttpContext.Session.TryGetValue("_files", out var filesBytes))
            {
                return RedirectToAction("Index", "Home");
            }
            var files = JsonConvert.DeserializeObject<List<MediaFile>>(Encoding.ASCII.GetString(filesBytes));
            var file = files.SingleOrDefault(x => x.Id == id);

            return View("Delete", file);
        }

        // POST: Media/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                if (!HttpContext.Session.TryGetValue("_files", out var filesBytes))
                {
                    return Delete(id);
                }
                var files = JsonConvert.DeserializeObject<List<MediaFile>>(Encoding.ASCII.GetString(filesBytes));
                var file = files.SingleOrDefault(x => x.Id == id);

                files.Remove(file);

                HttpContext.Session.Set("_files", Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(files)));

                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return Delete(id);
            }
        }
    }
}
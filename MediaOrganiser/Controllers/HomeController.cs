﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MediaOrganiser.Models;
using Newtonsoft.Json;
using System.Text;

namespace MediaOrganiser.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var model = new OrganiserModel();

            if (HttpContext.Session.TryGetValue("_files", out var files))
            {
                model.FileList = JsonConvert.DeserializeObject<List<MediaFile>>(Encoding.ASCII.GetString(files));
            }
            else
            {
                model.FileList = new List<MediaFile>
                {
                    new MediaFile{Id = 1, FileName = "Cerys", Path = "C:\\files", Type = ".fag", Comments = "Totally a fag"}
                };
                HttpContext.Session.Set("_files", Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(model.FileList)));
            }

            return View("index", model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
